const db = require('../helpers/database');

const validateBody = ({
  whoIsAzorAhai,
  whoIsNissaNissa,
  whoWillBeKing,
  whoWillKillCersei,
}) => {
  if (!whoIsAzorAhai || !whoIsNissaNissa || !whoWillBeKing || !whoWillKillCersei) {
    const result = {
      whoIsAzorAhai,
      whoIsNissaNissa,
      whoWillBeKing,
      whoWillKillCersei,
    };

    const entries = Object.entries(result);
    return entries.reduce((acc, curr) => {
      if (!curr[1]) return acc;
      const [key, value] = curr;
      acc[key] = value;
      return acc;
    }, {});
  }
  throw new Error('ValidationError');
};


const createAnswerBet = async (req, res) => {
  try {
    const { user } = req;

    const answerBet = validateBody(req.body);

    Object.assign(answerBet, { updateAt: new Date() });

    await db.collection('bets').doc(user.uid).update(answerBet);

    res.sendStatus(201);
  } catch (err) {
    console.log('Error on createAnswerBet', err); /* eslint-disable-line */
    if (err.message === 'ValidationError') {
      res.status(400).send('ValidationError');
    }
    throw err;
  }
};

module.exports = createAnswerBet;
