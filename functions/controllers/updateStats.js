const _ = require('lodash');
const db = require('../helpers/database');

const reduceBetsCharacters = bets => (
  bets.reduce((acc, { characters }) => [...acc, ...characters], [])
);

const computeCharacters = (bets) => {
  const allCharacters = reduceBetsCharacters(bets);

  const computedData = allCharacters.reduce((acc, character) => {
    const data = acc.find(char => char.id === character.id);
    if (data) {
      Object.assign(data, {
        saved: data.saved + (character.isAlive ? 1 : 0),
        killed: data.killed + (character.isAlive === false ? 1 : 0),
      });
    } else {
      acc.push({
        id: character.id,
        saved: character.isAlive ? 1 : 0,
        killed: character.isAlive === false ? 1 : 0,
      });
    }

    return acc;
  }, []);

  const mostKilledCharacters = _(computedData)
    .map((value) => {
      Object.assign(value, { count: value.killed });
      return _.omit(value, ['killed', 'saved']);
    })
    .orderBy(['count'], ['desc'])
    .value();

  const mostSavedCharacters = _(computedData)
    .map((value) => {
      Object.assign(value, { count: value.saved });
      return _.omit(value, ['killed', 'saved']);
    })
    .orderBy(['count'], ['desc'])
    .value();

  return {
    mostSavedCharacters,
    mostKilledCharacters,
  };
};

const computedBets = (bets, key) => {
  const computedData = bets.reduce((acc, bet) => {
    if (!bet[key]) return acc;

    const data = acc.find(char => char.id === bet[key]);
    if (data) {
      Object.assign(data, {
        count: data.count + 1,
      });
    } else {
      acc.push({
        id: bet[key],
        count: 0,
      });
    }

    return acc;
  }, []);

  return _(computedData).sortBy(['count']).reverse().value();
};

const updateStats = async (bets) => {
  try {
    const {
      mostSavedCharacters,
      mostKilledCharacters,
    } = computeCharacters(bets);

    const stats = {
      betsCount: bets.length,
      charactersVotesCount: reduceBetsCharacters(bets).length,
      mostKilledCharacters,
      mostSavedCharacters,
      whoWillBeKing: computedBets(bets, 'whoWillBeKing'),
      whoWillBeNissaNissa: computedBets(bets, 'whoIsNissaNissa'),
      whoWillBeAzorAhai: computedBets(bets, 'whoIsAzorAhai'),
      whoWillKillCersei: computedBets(bets, 'whoWillKillCersei'),
      createdAt: new Date(),
    };

    await db.collection('stats').add(stats);

    console.log('Stats updated with success'); /* eslint-disable-line */
  } catch (err) {
    console.log('Error on updateStats', err); /* eslint-disable-line */
    throw err;
  }
};

module.exports = updateStats;
