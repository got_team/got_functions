const db = require('../helpers/database');
const updateStats = require('./updateStats');

const POINTS = Object.freeze({
  ALIVE: 200,
  DEAD: 100,
  KING: 400,
  AZOR_AHAI: 300,
  NISSA_NISSA: 300,
  KILLED_CERSEI: 500,
});

const calculatePointsIsAlive = ({ isAlive: betCharacterIsAlive }, currentCharacter) => {
  if (betCharacterIsAlive === currentCharacter.isAlive && currentCharacter.isAlive !== null) {
    if (currentCharacter.isAlive) {
      return POINTS.ALIVE;
    }
    return POINTS.DEAD;
  }
  return 0;
};

const calculateWhoIsAzorAhaiPoints = ({ id, isAzorAhai }, bet, points) => {
  if (isAzorAhai && id === bet.whoIsAzorAhai && bet.whoIsAzorAhai !== null) {
    return points;
  }
  return 0;
};

const calculateWhoIsKingPoints = ({ id, isKing }, bet, points) => {
  if (isKing && id === bet.whoWillBeKing && bet.whoWillBeKing !== null) {
    return points;
  }
  return 0;
};

const calculateWhoIsNissaNissaPoints = ({ id, isNissaNissa }, bet, points) => {
  if (isNissaNissa && id === bet.whoIsNissaNissa && bet.whoIsNissaNissa !== null) {
    return points;
  }
  return 0;
};

const calculateWhoWillKillCerseiPoints = ({ id, killedCersei }, bet, points) => {
  if (killedCersei && id === bet.whoWillKillCersei && bet.whoWillKillCersei !== null) {
    return points;
  }
  return 0;
};

const calculateBetPoints = (character, bet) => {
  let points = 0;

  const currentCharacter = bet.characters.find(char => char.id === character.id);
  if (currentCharacter) {
    points += calculatePointsIsAlive(character, currentCharacter);
  }

  points += calculateWhoIsAzorAhaiPoints(character, bet, POINTS.AZOR_AHAI);
  points += calculateWhoIsKingPoints(character, bet, POINTS.KING);
  points += calculateWhoIsNissaNissaPoints(character, bet, POINTS.NISSA_NISSA);
  points += calculateWhoWillKillCerseiPoints(character, bet, POINTS.KILLED_CERSEI);

  return { id: bet.id, points };
};

const getBetsWithCharacters = async (bets) => {
  const betsWithCharactersPromises = bets.map(async (bet) => {
    const charactersSnapshot = await db.collection('bets')
      .doc(bet.id).collection('characters').get();
    const characters = charactersSnapshot.docs.map(character => character.data());
    return Object.assign({}, bet, { characters });
  });

  return Promise.all(betsWithCharactersPromises);
};

const getBets = async () => {
  const betsSnapshot = await db.collection('bets').orderBy('points', 'desc').orderBy('updateAt').get();
  return betsSnapshot.docs.map((doc) => {
    const data = doc.data();
    return Object.assign(data, { id: doc.id });
  });
};

const getCharacters = async () => {
  const charactersSnapshot = await db.collection('characters').get();
  const data = charactersSnapshot.docs.map(doc => doc.data());
  data.push({
    id: 'none',
    isAlive: null,
    isKing: false,
    isNissaNissa: true,
    isAzorAhai: true,
    killedCersei: false,
  });
  return data;
};

const updateBetsRankingPosition = async () => {
  const bets = await getBets();

  await Promise.all(bets.map((bet, index) => db.collection('bets').doc(bet.id).update({ position: index + 1 })));
  console.log('Bets Ranking Position updated with success'); /* eslint-disable-line */
};

const updateBetPoints = async (updatedBets) => {
  const updatedBetsPromises = updatedBets.map(({ id, points }) => (
    db.collection('bets').doc(id).update({ points })
  ));

  await Promise.all(updatedBetsPromises);
  console.log('Bets Points updated with success'); /* eslint-disable-line */
};

const updateBetPointsAndPosition = async (req, res) => {
  try {
    const { headers: { 'inner-authorization': auth } } = req;

    if (auth !== process.env.INNER_AUTHORIZATION) {
      throw new Error('Unauthorized');
    }

    const characters = await getCharacters();

    const bets = await getBets();

    const betsWithCharacters = await getBetsWithCharacters(bets);

    const updatedBets = betsWithCharacters.map((bet) => {
      const betCharactersPoints = characters.map(character => calculateBetPoints(character, bet));
      return betCharactersPoints.reduce((acc, curr) => ({
        id: curr.id,
        points: acc.points + curr.points,
      }), {
        points: 0,
      });
    });

    await updateBetPoints(updatedBets);

    await updateBetsRankingPosition();

    await updateStats(betsWithCharacters);

    res.sendStatus(200);
  } catch (err) {
    console.log('Error on updateBetPoints', err); /* eslint-disable-line */
    if (err.message === 'Unauthorized') {
      res.status(401).send('Unauthorized');
    }
    if (err.message === 'ValidationError') {
      res.status(400).send('ValidationError');
    }
    throw err;
  }
};

module.exports = updateBetPointsAndPosition;
