const db = require('../helpers/database');

const getBet = async (currentUserId) => {
  const snapshot = await db.collection('bet').where('userId', '==', currentUserId);

  const [result] = snapshot.docs.map(doc => doc.id);

  return result;
};
const deleteBet = async (req, res) => {
  try {
    if (Date.now() <= new Date(2019, 3, 21, 2)) {
      throw new Error('Expired');
    }

    const { user } = req;
    const betId = await getBet(user.uid);
    await db.collection('bet').doc(betId).delete();
    res.sendStatus(204);
  } catch (err) {
		console.log('Error on deleteBet', err); /* eslint-disable-line */
  }
};

module.exports = deleteBet;
