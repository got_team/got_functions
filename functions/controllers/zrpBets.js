const admin = require('firebase-admin');
const _ = require('lodash');
const db = require('../helpers/database');

const usersEmails = [
  'kidjudoca@gmail.com',
  'rafcostella@gmail.com',
  'pedro.gryzinsky@zrp.com.br',
  'jose.almeida@zrp.com.br',
  'guga.rodarte@hotmail.com',
];

const getUsersUids = async () => {
  const users = await Promise.all(usersEmails.map(email => admin.auth().getUserByEmail(email)));
  return users.map(user => user.uid);
};

const getBets = async (uids) => {
  const betsSnapshots = await Promise.all(uids.map(uid => db.collection('bets').where('userId', '==', uid).get()));

  const bets = betsSnapshots.map((bet) => {
    const [data] = bet.docs.map(doc => doc.data());
    const updateAt = new Date(data.updateAt.seconds * 1000);
    return Object.assign({}, data, { updateAt });
  });

  const betsOrdered = _.orderBy(bets, ['points', 'updateAt'], ['desc', 'asc']);

  return betsOrdered;
};

const zrpBets = async (req, res) => {
  try {
    const uids = await getUsersUids();
    const bets = await getBets(uids);

    res.send(bets);
  } catch (err) {
    throw err;
  }
};

module.exports = zrpBets;
