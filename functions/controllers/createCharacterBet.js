const db = require('../helpers/database');

const betExists = async ({
  uid,
  name,
  picture,
}) => {
  const snapshot = await db.collection('bets').where('userId', '==', uid).get();
  if (snapshot.docs.length === 0) {
    await db.collection('bets').doc(uid).set({
      points: 0,
      updateAt: new Date(),
      userId: uid,
      displayName: name,
      photoURL: picture,
      whoIsAzorAhai: null,
      whoIsNissaNissa: null,
      whoWillBeKing: null,
      whoWillKillCersei: null,
    });
  }
};

const validateBody = ({
  isAlive,
  id,
}) => {
  if (isAlive === undefined || isAlive === null || !id) {
    throw new Error('ValidationError');
  }
  return {
    isAlive,
    id,
  };
};


const createCharacterBet = async (req, res) => {
  try {
    const { user } = req;
    await betExists(user);

    const characterBet = validateBody(req.body);

    Object.assign(characterBet, { updateAt: new Date() });

    await db.collection('bets').doc(user.uid)
      .collection('characters').add(characterBet);

    res.sendStatus(201);
  } catch (err) {
    console.log('Error on createCharacterBet', err); /* eslint-disable-line */
    if (err.message === 'ValidationError') {
      res.status(400).send('ValidationError');
    }
    throw err;
  }
};

module.exports = createCharacterBet;
