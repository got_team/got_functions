require('dotenv').load();
const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');


const cors = require('cors')({ origin: true });
const createCharacterBet = require('./controllers/createCharacterBet');
const createAnswerBet = require('./controllers/createAnswerBet');
const updateBetPointsAndPosition = require('./controllers/updateBetPointsAndPosition');
const zrpBets = require('./controllers/zrpBets');
const validateFirebaseIdToken = require('./helpers/validateFirebaseIdToken');

const app = express();

app.use(cors);
// Automatically allow cross-origin requests
app.use(bodyParser.json());

app.get('/status', (req, res) => res.sendStatus(200));
// build multiple CRUD interfaces:
app.get('/zrp', zrpBets);

app.post('/characters', validateFirebaseIdToken, createCharacterBet);
app.post('/answers', validateFirebaseIdToken, createAnswerBet);
app.post('/points', updateBetPointsAndPosition);


app.use((err, req, res) => {
  console.log(err); /* eslint-disable-line */
  res.status(500).send('Something is broken');
});

exports.app = functions.https.onRequest(app);

exports.staging = functions.https.onRequest(app);
